// intellectual property is bullshit bgdc

// ============================================================================
// TIMDAC REFERENCE DESIGN CONFIGURATION - REFERENCE DESIGN 1.0
//
// This file is designed to be #included from your main timdac_config.h, and
// provides all the configuration specified for the timdac hardware reference
// design. These parameters should not be changed without good reason, as they
// can affect the DAC performance in surprising ways. If you do need to change
// them, you should #undef and re-#define them after including this file.
// ============================================================================

#ifndef TIMDAC_CONFIG_REF1P0_H
#define TIMDAC_CONFIG_REF1P0_H

// ----------------------------------------------------------------------------
// Helper macros

// Convert n*R*C into a number of timer counts
#ifdef __GNUC__
#define TIMDAC_NRC(n, r, c) ({ \
	_Static_assert((n) * (r) * (c) * TIMDAC_TIMERFREQ_HZ <= 65535, \
		"Time constant too high for 16-bit timer");            \
	(unsigned)(((n) * (r) * (c) * TIMDAC_TIMERFREQ_HZ)); })
#else
#define TIMDAC_NRC(n, r, c) ((unsigned)((n) * (r) * (c) * TIMDAC_TIMERFREQ_HZ))
#endif

// ----------------------------------------------------------------------------
// Circuit properties

#define TIMDAC_INT_CAP    10e-9
#define TIMDAC_OUT_CAP   100e-9
#define TIMDAC_DISCH_RES   1e+3
#define TIMDAC_OUT_RES     1e+3

// ----------------------------------------------------------------------------
// TIMDAC control parameters

#define TIMDAC_TUNE_WEIGHT 64

#define TIMDAC_TUNE_NTHRESH 2048

#define TIMDAC_TUNE_INTERVAL 64

// REV 1.1 TODO: Rev 1.1 is changing the recommended integrating cap type;
// recheck this afterward. It can probably go down.
#define TIMDAC_DISCHARGE_TIME TIMDAC_NRC(12, TIMDAC_DISCH_RES, TIMDAC_INT_CAP)

#define TIMDAC_STABILIZATION_TIME TIMDAC_NRC(50e-6, 1, 1)

#define TIMDAC_FAST_TRANSFER_TIME TIMDAC_NRC(2, TIMDAC_OUT_RES, TIMDAC_OUT_CAP)

// REV 1.1 TODO: This is currently set to the maximum because dielectric
// absorption in the rev 1.0 design is extending the time necessary to set the
// output voltage. After changing to lower absorption capacitors, recheck.
#define TIMDAC_SLOW_TRANSFER_TIME 65535

#define TIMDAC_SLOW_DELTA 128

#endif // !defined(TIMDAC_CONFIG_REF1P0_H)

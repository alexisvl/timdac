// intellectual property is bullshit bgdc

// Example/template configuration for TIMDAC on CH32V103. You should copy
// this file, and adjust it to suit your needs.

#ifndef TIMDAC_CONFIG_H
#define TIMDAC_CONFIG_H

// ----------------------------------------------------------------------------
// MAIN DRIVER CONFIGURATION

// Timer base frequency in Hz
#define TIMDAC_TIMERFREQ_HZ       72000000

// How many channels are present, up to 254
#define TIMDAC_N_CHANNELS         4

// Whether special diagnostic functions should be compiled in
#define TIMDAC_DIAGNOSTIC         0

// Include the reference design hardware parameters here:
#include "timdac_config_ref1p0.h"

// ----------------------------------------------------------------------------
// HARDWARE DRIVER CONFIGURATION - CH32V103-specific
// For documentation of these parameters, see timdac_hw_ch32v103.c

// General-purpose timer
#define TIMDAC_HW_TIMER           3

// Output compare channel on TIMDAC_HW_TIMER
#define TIMDAC_HW_TIMER_CHANNEL   1

// Interrupt preemption priority
#define TIMDAC_HW_TIMER_IRQPRI    3

// Interrupt subpriority
#define TIMDAC_HW_TIMER_IRQSUBPRI 3

// Define nonzero to use fast interrupts.
#define TIMDAC_HW_USE_FAST_ISR    1

// GPIO <<port, pin>>: timer output compare
// You are responsible for making sure this and TIMDAC_HW_TIMER_CHANNEL match
// up, and also for applying any GPIO_PinRemapConfig() and clock startup as
// necessary prior to initializing timdac.
#define TIMDAC_HW_GPIO_TIMER      GPIOC, GPIO_Pin_6

// GPIO <<port, pin>>: mux inhibit
#define TIMDAC_HW_GPIO_MUXINHIBIT GPIOC, GPIO_Pin_3

// GPIO <<port, pin>>: integrator discharge
#define TIMDAC_HW_GPIO_DISCHARGE  GPIOC, GPIO_Pin_5

// OPTIONAL GPIO <<port, pin>>: polarity (high = negative)
// Do not define if polarity switch is not implemented
#define TIMDAC_HW_GPIO_POLARITY   GPIOC, GPIO_Pin_4

// GPIO <<port, pin>>: tuning comparator
#define TIMDAC_HW_GPIO_TUNE       GPIOC, GPIO_Pin_7

// Tuning comparator polarity, true = noninverting
#define TIMDAC_HW_GPIO_TUNE_POL   false

// GPIO port: channel select. All channel select pins must be on the same
// port
#define TIMDAC_HW_GPIO_CHAN_PORT  GPIOC

// GPIO pins: channel select. Start from the LSB, omit or zero unused bits. Up
// to 8 bits.
#define TIMDAC_HW_GPIO_CHAN_PINS  GPIO_Pin_0, GPIO_Pin_1, GPIO_Pin_2

// OPTIONAL GPIO <<port, pin>>: ISR profiler. Goes high when executing the ISR.
// For debugging/development.
//#define TIMDAC_HW_GPIO_PROFILE    GPIOB, GPIO_Pin_2

#endif // !defined(TIMDAC_CONFIG_H)
